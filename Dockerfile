FROM python:3.8-slim-buster

RUN apt-get update && apt-get install -y \
    wget \
    unzip \
    vim \
    tmux \
    make \
    build-essential

RUN pip install chess

RUN wget https://files.stockfishchess.org/archive/Stockfish%2015/stockfish_15_linux_x64_avx2.zip -P /home/ && \
    unzip /home/stockfish_15_linux_x64_avx2.zip -d /home

RUN cp -r /home/stockfish_15_linux_x64_avx2/* /home/

RUN cd /home/stockfish_15_src/src && \
    make build ARCH=x86-64-modern && \
    cp /home/stockfish_15_src/src/stockfish /home/stockfish

ADD chess_server.py /home/chess_server.py
